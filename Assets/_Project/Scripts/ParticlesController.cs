﻿using UnityEngine;

public class ParticlesController : MonoBehaviour
{
    [SerializeField] private ParticleSystem _waterParticles;
    [SerializeField] private ParticleSystem[] _confetties;

    private void Start()
    {
        Actions.OnJumpedInWater += TriggerWaterParticles;
        Actions.OnFinish += TriggerConfetties;
    }

    private void TriggerConfetties(bool won)
    {
        if (!won)
            return;
        
        foreach (var confetty in _confetties)
        {
            confetty.Play();
        }
    }
    
    private void TriggerWaterParticles(Vector3 position)
    {
        _waterParticles.transform.position = position;
        _waterParticles.Play();
    }

    private void OnDestroy()
    {
        Actions.OnJumpedInWater -= TriggerWaterParticles;
        Actions.OnFinish -= TriggerConfetties;
    }
}
