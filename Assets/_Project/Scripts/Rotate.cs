﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    [SerializeField] private float rotateSpeed;
    private Transform _transform;

    private void Start()
    {
        _transform = GetComponent<Transform>();
    }

    void Update()
    {
        _transform.Rotate(new Vector3(0f,0f,1f * Time.deltaTime * rotateSpeed ));
    }
}
