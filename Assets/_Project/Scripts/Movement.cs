﻿using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private float _movingSpeed;
    [SerializeField] private float _rotationSpeed;
    private Transform _transform;
    private Rigidbody _rigidbody;

    void Start()
    {
        _transform = GetComponent<Transform>();
        _rigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
    }

    public void Rotate(float angle)
    {
        _transform.Rotate(0f,-angle * _rotationSpeed,0f);
    }
    
    public void Move(Vector3 direction)
    {
        _rigidbody.position = Vector3.Lerp(_transform.position, _transform.position + direction, Time.deltaTime * _movingSpeed);
    }
}
