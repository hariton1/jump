﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class WaterPlatform : IPlatform
{
    protected override void OnInteract()
    {
        var parent = transform.parent;
        parent.DOMove(parent.position - new Vector3(0f, 2f, 0f), 3f);
    }
}
