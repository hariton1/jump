﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceLineController : MonoBehaviour
{
    private LineRenderer _lineRenderer;

    private void Start()
    {
        _lineRenderer = GetComponent<LineRenderer>();
        Actions.OnFinish += OnFinish;
    }

    private void OnFinish(bool won)
    {
        _lineRenderer.enabled = false;
    }
    
    private void Update()
    {
        UpdateDistance();
    }

    private void UpdateDistance()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -transform.up, out hit, 500f, ~8))
        {
            if (hit.collider.GetComponent<IPlatform>() || hit.collider.GetComponent<FinishTrigger>())
            {
                _lineRenderer.startColor = new Color(0f, 1f, 1f, .5f);
                _lineRenderer.endColor =  new Color(0f, 1f, 1f, .5f);
            }
            else
            {
                _lineRenderer.startColor =  new Color(1f, 0f, 0f, .5f);
                _lineRenderer.endColor = new Color(1f, 0f, 0f, .5f);
            }
            _lineRenderer.SetPosition(1,hit.point - transform.position);
        }
    }

    private void OnDestroy()
    {
        Actions.OnFinish -= OnFinish;
    }
}
