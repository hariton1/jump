﻿using System;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public abstract class IPlatform : MonoBehaviour
{
    [SerializeField] private ParticleSystem _onCollisionParticles;
    [SerializeField] protected float jumpForce;
    private Collider _collider;

    private void Start()
    {
        OnStart();
    }

    private void TriggerParticles()
    {
        _onCollisionParticles.Play();
    }

    protected virtual void OnStart()
    {
        _collider = GetComponent<Collider>();
    }

    public void OnCollisionEnter(Collision other)
    {
        var _jumpForce = jumpForce;
        var _wasPerfectJump = false;
        var objectThatJumps = other.gameObject.GetComponent<IJump>();

        if (objectThatJumps != null)
        {
            if (CheckIfCollidedInCenter(other))
            {
                _jumpForce *= 1.2f;
                _wasPerfectJump = true;
                Actions.OnJumpedInCenter();
            }

            TriggerParticles();
            Vibration.VibratePop();
            objectThatJumps.Jump(_jumpForce,_wasPerfectJump);
            Actions.OnJumpedOnPlatform(this);
            OnInteract();
        }
    }

    private bool CheckIfCollidedInCenter(Collision other)
    {
        var platformCenterPoint = _collider.bounds.center;
        platformCenterPoint.y = _collider.bounds.max.y;
        if (Vector3.Distance(other.GetContact(0).point, platformCenterPoint) < _collider.bounds.extents.x / 2.8f)
        {
            return true;
        }

        return false;
    }

    protected virtual void OnInteract()
    {
        
    }
}
