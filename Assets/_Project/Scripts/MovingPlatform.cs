﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class MovingPlatform : IPlatform
{
    [SerializeField] private Vector3 _movingDirection;
    [SerializeField] private float _movingDistance;
    [SerializeField] private float _animationDuration;
    private Transform _parentTransform;
    private Vector3 _initialPosition;

    protected override void OnStart()
    {
        base.OnStart();
        _parentTransform = transform.parent;
        _initialPosition = _parentTransform.position;
        StartCoroutine(Move());
    }

    private IEnumerator Move()
    {
        while (true)
        {
            _parentTransform.DOMove(_initialPosition + (_movingDirection * _movingDistance),_animationDuration).SetEase(Ease.Linear);
            yield return new WaitForSeconds(_animationDuration);
            _parentTransform.DOMove(_initialPosition + (-_movingDirection * _movingDistance),_animationDuration * 2).SetEase(Ease.Linear);
            yield return new WaitForSeconds(_animationDuration * 2);
            _parentTransform.DOMove(_initialPosition,_animationDuration).SetEase(Ease.Linear);
            yield return new WaitForSeconds(_animationDuration);

        }
    }
}
