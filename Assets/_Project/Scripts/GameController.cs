﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private GameObject _platformsHolder;
    private IPlatform[] _allPlatforms;

    private void Start()
    {
        Actions.OnJumpedOnPlatform += OnJumpedOnPlatform;
        Actions.CreateLinesBetweenPlatforms(_platformsHolder.transform);
        ObtainAllPlatforms();
    }

    private void ObtainAllPlatforms()
    {
        var platforms = _platformsHolder.GetComponentsInChildren<IPlatform>();
        _allPlatforms = new IPlatform[platforms.Length];
        _allPlatforms = platforms;
    }

    private void OnJumpedOnPlatform(IPlatform platform)
    {
        for (int i = 0; i < _allPlatforms.Length; i++)
        {
            if (platform == _allPlatforms[i])
            {
                Actions.OnUpdateProgressBar((i + 1f) /_allPlatforms.Length);
            }
        }
    }

    private void OnDestroy()
    {
        Actions.OnJumpedOnPlatform -= OnJumpedOnPlatform;
    }
}
