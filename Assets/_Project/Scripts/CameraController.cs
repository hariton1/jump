﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using NaughtyAttributes;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [HideInInspector] [SerializeField] private Point _initialPoint;
    [HideInInspector] [SerializeField] private Point _intermediatePoint;
    [HideInInspector] [SerializeField] private Point _defaultPoint;
    private Camera _camera;
    
    private void Start()
    {
        _camera = GetComponent<Camera>();
        
        ReturnToInitialPoint();
        Actions.OnStart += OnStart;
        Actions.OnFinish += OnFinish;
    }

    private void OnFinish(bool won)
    {
        if(!won)
            return;

        // transform.DOLocalRotateQuaternion(_intermediatePoint.localRotation, 1f).SetEase(Ease.Linear);
        // transform.DOLocalMove(_intermediatePoint.localPosition, 1f).SetEase(Ease.Linear).onComplete = () =>
        // {
        //     var newMask = _camera.cullingMask & ~(1 << 9);
        //     _camera.cullingMask = newMask;
        //     
        //     transform.DOLocalRotateQuaternion(_initialPoint.localRotation, 1f).SetEase(Ease.Linear);
        //     StartCoroutine(Utils.WaitForSeconds(0.1f, () =>
        //     {
        //         transform.DOLocalMove(_initialPoint.localPosition, 1f).SetEase(Ease.Linear);
        //     }));
        // };
        
        
        var newMask = _camera.cullingMask & ~(1 << 9);
        _camera.cullingMask = newMask;
        Vector3[] positions = new[] {_defaultPoint.localPosition, _intermediatePoint.localPosition, _initialPoint.localPosition};
        transform.DOLocalPath(positions, 4f, PathType.CatmullRom);
        transform.DOLocalRotateQuaternion(_initialPoint.localRotation, 4f);

    }

    private void OnStart()
    {
        transform.DOLocalRotateQuaternion(_defaultPoint.localRotation, .7f);
        StartCoroutine(Utils.WaitForSeconds(0.1f, () =>
        {
            transform.DOLocalMove(_defaultPoint.localPosition, .7f).SetEase(Ease.Linear).onComplete = () =>
            {
                Actions.EnableControls(true);
            };
        }));
    }


    [Button()]
    public void SaveDefaultPoint()
    {
        _defaultPoint.localRotation = transform.localRotation;
        _defaultPoint.localPosition = transform.localPosition;
    }

    [Button()]
    private void SaveInitialPoint()
    {
        _initialPoint.localRotation = transform.localRotation;
        _initialPoint.localPosition = transform.localPosition;
    }
    
    [Button()]
    private void SaveIntermediatePoint()
    {
        _intermediatePoint.localRotation = transform.localRotation;
        _intermediatePoint.localPosition = transform.localPosition;
    }

    [Button()]
    private void ReturnToDefaultPoint()
    {
        transform.localRotation = _defaultPoint.localRotation;
        transform.localPosition = _defaultPoint.localPosition;
    }
    
    [Button()]
    private void ReturnToInitialPoint()
    {
        transform.localRotation = _initialPoint.localRotation;
        transform.localPosition = _initialPoint.localPosition;
    }

    private void OnDestroy()
    {
        Actions.OnStart -= OnStart;
        Actions.OnFinish -= OnFinish;
    }
}

[Serializable]
public struct Point
{
    public Vector3 localPosition;
    public Quaternion localRotation;
}
