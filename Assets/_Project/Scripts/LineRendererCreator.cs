﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRendererCreator : MonoBehaviour
{
    
    private LineRenderer _lineRenderer;

    private void Awake()
    {
        _lineRenderer = GetComponent<LineRenderer>();
        Actions.CreateLinesBetweenPlatforms += CreateRenderer;

    }
    
    private void CreateRenderer(Transform parent)
    {
        var firstPlatform = parent.GetChild(0);
        for (int i = 0; i < parent.childCount; i++)
        {
            var difference = parent.GetChild(i).localPosition - firstPlatform.localPosition;
            _lineRenderer.positionCount++;
            _lineRenderer.SetPosition(_lineRenderer.positionCount - 1,difference);
        }
    }

    private void OnDestroy()
    {
        Actions.CreateLinesBetweenPlatforms -= CreateRenderer;
    }
}
