﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillTrigger : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        var isPlayer = other.gameObject.GetComponent<IKillable>();

        if (isPlayer != null)
        {
            isPlayer.Die();
        }
    }
}
