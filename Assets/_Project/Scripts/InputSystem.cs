﻿using System;
using UnityEngine;

public class InputSystem : MonoBehaviour
{
    [SerializeField] private Movement _playerMovement;
    private Vector3 _initialMousePosition;
    private Vector3 _currentMousePosition;
    private bool _canControlPlayer;

    private void Start()
    {
        Actions.EnableControls += OnStart;
    }

    private void OnStart(bool value)
    {
        _canControlPlayer = value;
    }
    
    
    private void Update()
    {
        if (!_canControlPlayer)
            return;
        
        if (Input.GetMouseButtonDown(0))
        {
            _initialMousePosition = Input.mousePosition;
        }

        if (Input.GetMouseButton(0))
        {
            if (_initialMousePosition == Vector3.zero)
                return;
            
            _playerMovement.Move(_playerMovement.transform.forward);
            _currentMousePosition = Input.mousePosition;
            var angleToRotate = (_currentMousePosition.x - _initialMousePosition.x) / Screen.width;
            _playerMovement.Rotate(angleToRotate);
            _initialMousePosition = _currentMousePosition;
        }
    }

    private void OnDestroy()
    {
        Actions.EnableControls -= OnStart;
    }
}
