﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utils : MonoBehaviour
{
    public static IEnumerator WaitForSeconds(float time,Action action)
    {
        yield return new WaitForSeconds(time);
        action();
    }
}
