﻿using System;
using UnityEngine;

public class Actions : MonoBehaviour
{
    public static Action OnJumpedInCenter;

    public static Action<bool> EnableControls;

    public static Action<Vector3> OnJumpedInWater;

    public static Action<bool> OnFinish;

    public static Action<Transform> CreateLinesBetweenPlatforms;
    
    public static Action<IPlatform> OnJumpedOnPlatform;

    public static Action<float> OnUpdateProgressBar;
    
    public static Action OnStart;
    
}
