﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Player : MonoBehaviour, IJump, IKillable
{

    [SerializeField] private ParticleSystem[] _particles;
    [SerializeField] private LineRenderer _lineRenderer;
    private Rigidbody _rigidbody;
    private Collider _collider;
    private Animator _animator;

    //ragdoll
    private List<Rigidbody> _ragdollRigidbodies = new List<Rigidbody>();
    private List<Collider> _ragdollColliders = new List<Collider>();
    //
    
    private static readonly int Front1 = Animator.StringToHash("Front 1");
    private static readonly int Front2 = Animator.StringToHash("Front 2");
    private static readonly int Back1 = Animator.StringToHash("Back 1");
    private static readonly int Dance = Animator.StringToHash("Dance");

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _animator = GetComponent<Animator>();
        _collider = GetComponent<Collider>();

        FindAllRagdollRigidbodies();
        EnableRagdollRigidbodies(false);
    }
    
    private void FindAllRagdollRigidbodies()
    {
        var allRigidbodies = GetComponentsInChildren<Rigidbody>();
        var allColliders = GetComponentsInChildren<Collider>();

        foreach (var rig in allRigidbodies)
        {
            if (rig != _rigidbody)
            {
                _ragdollRigidbodies.Add(rig);
            }
        }

        foreach (var collider in allColliders)
        {
            if (collider != _collider)
            {
                _ragdollColliders.Add(collider);
            }
        }
    }

    private void EnableRagdollRigidbodies(bool value)
    {
        foreach (var rig in _ragdollRigidbodies)
        {
            rig.useGravity = value;
            rig.isKinematic = !value;
        }

        foreach (var collider in _ragdollColliders)
        {
            collider.enabled = value;
        }
    }

    private void TriggerParticles()
    {
        for (int i = 0; i < _particles.Length; i++)
        {
            _particles[i].Play();
        }
    }

    private void TriggerAFlipAnimation()
    {
        int[] animations = {Front1, Front2,Back1};
        var randomAnimationID = Random.Range(0, animations.Length);
        _animator.SetTrigger(animations[randomAnimationID]);
    }

    public void Jump(float force,bool wasPerfectJump)
    {
        if (wasPerfectJump)
        {
            TriggerParticles();
        }
        
        _rigidbody.AddForce(new Vector3(0f,1f * force,0f),ForceMode.Acceleration);
        TriggerAFlipAnimation();
    }

    public void Die()
    {
        Actions.OnFinish(false);
        Actions.EnableControls(false);
        Vibration.VibrateNope();
        Destroy(_collider);
        Destroy(_rigidbody);
        Destroy(_animator);
        Destroy(_lineRenderer.gameObject);
        EnableRagdollRigidbodies(true);
        Destroy(this);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Water"))
        {
            var waterParticlesPosition = transform.position;
            waterParticlesPosition.y = other.transform.position.y;
            Actions.OnJumpedInWater(waterParticlesPosition);
            Die();
        }

        if (other.gameObject.GetComponent<FinishTrigger>())
        {
            Actions.EnableControls(false);
            Actions.OnFinish(true);
            _animator.SetTrigger(Dance);
            Vibration.VibratePop();
        }
    }

    private void OnDestroy()
    {
    }
}
