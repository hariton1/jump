﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyablePlatform : IPlatform
{
    [SerializeField] private List<Transform> _fragmentedObjectsParents = new List<Transform>();
    [SerializeField] private List<Transform> _defaultObjects = new List<Transform>();
    
    protected override void OnInteract()
    {
        FragmentObject();
    }

    private void FragmentObject()
    {
          
        foreach (var fragmentedObjectsParent in _fragmentedObjectsParents)
        {
            for (int i = 0; i < fragmentedObjectsParent.childCount; i++)
            {
                var obj = fragmentedObjectsParent.GetChild(i).gameObject;
                obj.GetComponent<Rigidbody>().useGravity = true;
                obj.GetComponent<Collider>().isTrigger = false;
                Destroy(obj,3f);
            }
        }

        foreach (var defaultObject in _defaultObjects)
        {
            Destroy(defaultObject.gameObject);
        }
      
    }
}
