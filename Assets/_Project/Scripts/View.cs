﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class View : MonoBehaviour
{
    [SerializeField] private String[] _encouragingWords;
    
    [Header("Panels")]
    [SerializeField] private GameObject _startPanel;
    [SerializeField] private GameObject _finishPanelWin;
    [SerializeField] private GameObject _finishPanelLose;
    [SerializeField] private GameObject _gamePlayPanel;
    
    [Header("Texts")]
    [SerializeField] private TextMeshProUGUI _encouragingWord;

    [Header("Buttons")]
    [SerializeField] private Button _startButton;
    [SerializeField] private Button _finishButtonWin;
    [SerializeField] private Button _finishButtonLose;

    [SerializeField] private Image _fillBar;

    
    private Coroutine _encouraginWordRoutine;

    private void Start()
    {
        Actions.OnUpdateProgressBar += UpdateProgressBar;
        Actions.OnJumpedInCenter += ShowEncouragingWord;
        Actions.OnFinish += OnFinish;
        
        _startButton.onClick.AddListener(() =>
        {
            _startPanel.gameObject.SetActive(false);
            _gamePlayPanel.gameObject.SetActive(true);
            Actions.OnStart();
        });
        
        _finishButtonWin.onClick.AddListener(() =>
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        });
        
        _finishButtonLose.onClick.AddListener(() =>
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        });
    }

    private void OnFinish(bool won)
    {
        _gamePlayPanel.gameObject.SetActive(false);
        
        if(won)
            _finishPanelWin.gameObject.SetActive(true);
        else _finishPanelLose.gameObject.SetActive(true);
    }

    private void UpdateProgressBar(float value)
    {
        _fillBar.DOFillAmount(value,1f);
    }

    private void ShowEncouragingWord()
    {
        if (_encouraginWordRoutine != null)
        {
            StopCoroutine(_encouraginWordRoutine);
        }
        
        _encouraginWordRoutine = StartCoroutine(EncouragingWord_Routine());
    }

    private IEnumerator EncouragingWord_Routine()
    {
        var word = _encouragingWords[Random.Range(0, _encouragingWords.Length)];
        _encouragingWord.text = word;
        _encouragingWord.DOColor(Color.yellow, 0.3f);
        yield return new WaitForSeconds(2f);
        _encouragingWord.DOColor(new Color(0f, 0f, 0f, 0f), .3f);
    }

    private void OnDestroy()
    {
        Actions.OnUpdateProgressBar -= UpdateProgressBar;
        Actions.OnJumpedInCenter -= ShowEncouragingWord;
        Actions.OnFinish -= OnFinish;
    }
}
